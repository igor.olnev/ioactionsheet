# How an ActionSheet

## 1 Create an IOActionSheet
```swift
let actionSheet = IOActionSheet()
let actionSheet = IOActionSheet(title: "Action sheet with title", 
                            font: UIFont = UIFont.boldSystemFont(ofSize: 13),
                            color: UIColor = .gray)
```

## 2 Define action sheet with characteristics

### 2.1 Configure
```swift
actionSheet.backgroundColor = UIColor.init(white: 1.0, alpha: 0.2)

// Cancel button customs
actionSheet.cancelButtonTitle = "Cancel"
actionSheet.cancelButtonBackgroundColor = .red
actionSheet.cancelButtonTextColor = .white

actionSheet.buttonsFont = UIFont.systemFont(ofSize: 15) // default font
```

### 2.2 Add actions
```swift
actionSheet.addButton(title: "Button 1",
                    font: UIFont.systemFont(ofSize: 15), /* Optional */
                    color: .red,                         /* Optional */
                    action: { [unowned self] in
                        self.actionSheetTest()
})
```

## 3 Present on view
```swift
actionSheet.show()
```
