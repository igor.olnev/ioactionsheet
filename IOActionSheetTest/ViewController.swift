//
//  ViewController.swift
//
//  Created by Igor Olnev on 05/08/2019.
//  Copyright © 2019 Igor Olnev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        bottomSheetTest()
    }
    
    public func bottomSheetTest() {
        let bottomSheet = IOActionSheet(title: "Test", font: UIFont.systemFont(ofSize: 15), color: .darkGray)
        
        bottomSheet.cancelButtonTitle = "Отменить"
        
        bottomSheet.buttonsFont = UIFont.systemFont(ofSize: 15)
        bottomSheet.backgroundColor = UIColor.init(white: 1.0, alpha: 0.2)
        
        bottomSheet.addButton(title: "Button 1",
                              action: { [unowned self] in
                                self.bottomSheetTest()
        })
        
        bottomSheet.addButton(title: "Button 2",
                              action: { [unowned self] in
                                self.bottomSheetTest()
        })
        
        bottomSheet.addButton(title: "Button 3",
                              action: { [unowned self] in
                                self.bottomSheetTest()
        })
        
        bottomSheet.addButton(title: "Button 4", action: {})
        
        bottomSheet.show()
    }
}

