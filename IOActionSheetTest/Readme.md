# How an ActionSheet

## 1 Create and Action sheet
```swift
let actionSheet = ActionSheet()
```

## 2 Define action sheet with characteristics

### 2.1 Configure
```swift
    actionSheet.backgroundColor = UIColor.init(white: 1.0, alpha: 0.2)

    // Cancel button customs
    actionSheet.cancelButtonTitle = "Cancel"
    actionSheet.cancelButtonBackgroundColor = .red
    actionSheet.cancelButtonTextColor = .white
    
    actionSheet.buttonsFont = UIFont.systemFont(ofSize: 15) // default font
```

### 2.2 Add actions
```swift
    actionSheet.addButton(title: "Button 1",
                        font: UIFont.systemFont(ofSize: 15), /* Optional */
                        color: .red,                         /* Optional */
                        action: { [unowned self] in
                            self.actionSheetTest()
    })
```

## 3 Present on view
```swift
    alert.show()
```

# На практике

```swift
    let actionSheet = ActionSheet()
    actionSheet.cancelButtonTitle = actionAttributedTitle(for: .cancel)
    
    actionSheet.addButton(title: actionAttributedTitle(for: .email),
                        action: { [unowned self] in
                        self.openApp(for: .email)
    })

    actionSheet.addButton(title: actionAttributedTitle(for: .gmail),
                        action: { [unowned self] in
                        self.openApp(for: .email)
    })

    actionSheet.show()
}
```
### Заменив старый код в DraftSaving.swift

```swift
    let actionSheet = AlertController(attributedTitle: nil, attributedMessage: nil,
    preferredStyle: .actionSheet)
    let attributedTitle = NSAttributedString(string: R.string.authorization.emailAppsTitle(),

    font: .caption, foregroundColor: .textPrimary)
    makeTitleLabel(for: actionSheet, withTitle: attributedTitle)
    let openEmailAction = AlertAction(attributedTitle: actionAttributedTitle(for: .email),

    style: .preferred) { [unowned self] _ in
        self.openApp(for: .email)
    }
    let openGmailAction = AlertAction(attributedTitle: actionAttributedTitle(for: .gmail),

    style: .preferred) { [unowned self]  _ in
        self.openApp(for: .gmail)
    }
    let cancelAction = AlertAction(attributedTitle: actionAttributedTitle(for: .cancel),

    style: .preferred)

    actionSheet.addAction(cancelAction)
    actionSheet.addAction(openEmailAction)
    actionSheet.addAction(openGmailAction)
    actionSheet.present()
}
```
